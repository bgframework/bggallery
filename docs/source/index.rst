.. BGGallery documentation master file, created by
   sphinx-quickstart on Thu Sep 20 17:08:12 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to BGGallery's documentation!
=====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   bbgplots/index.rst
   examples_mpl/index.rst
   examples_bkh/index.rst



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
