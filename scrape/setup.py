from os import path
from setuptools import setup, find_packages


directory = path.dirname(path.abspath(__file__))
with open(path.join(directory, 'requirements.txt')) as f:
    required = f.read().splitlines()


setup(
    name='scrape',
    version='0.2',
    description='BBGLab tool',
    packages=find_packages(),
    install_requires=required
)