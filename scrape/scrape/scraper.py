import collections

from bokeh import io as bkh_io
from bokeh.io import export_png as bkh_png
from bokeh.models import LayoutDOM as BkhFigure
from matplotlib.figure import Figure as MplFigure
from sphinx_gallery import scrapers


_queue = collections.deque()

show = _queue.append


bkh_io.show = show  # override bokeh show function to capture the plots and convert them to png


def mpl_save(fig, *args, **kwargs):
    from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
    FigureCanvas(fig)
    fig.savefig(*args, **kwargs)


def bkh_save(figure, file):
    bkh_png(figure, filename=file)


def save(figure, path):
    if isinstance(figure, MplFigure):
        return mpl_save(figure, path)
    elif isinstance(figure, BkhFigure):
        return bkh_save(figure, path)
    else:
        raise NotImplementedError('Figure of class %s cannot be saved' % figure.__class__)


def scraper(block, block_vars, gallery_conf):
    image_path_iterator = block_vars['image_path_iterator']
    image_paths = list()
    while _queue:
        image_path = image_path_iterator.next()
        save(_queue.popleft(), image_path)
        image_paths.append(image_path)
    return scrapers.figure_rst(image_paths, gallery_conf['src_dir'])
