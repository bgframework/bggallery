======
scrape
======

Scrapper to include matplotlib (not pyplot figures) and bokeh figures (as PNG images)
for sphinx-gallery.

Requires to use the **show** function in your plot script. E.g.:

.. code:: python

    from bokeh.plotting import figure
    from scrape import show

    p = figure()
    ... # do your plot
    show(p)

.. warning:: This function is intented to be used only within **bggallery**.
   Remove it if you are outside.

