"""
Customizing your lines
======================

Customize your lines with the color, linestyle, linewidth or marker properties,
amongst others

"""

import matplotlib.pyplot as plt
import numpy as np

y0 = [1, 1, 1, 1]
y1 = [3, 5, 9, 6, 4]
x1 = np.arange(len(y1))
y2 = [5, 5, 6, np.nan, 7, 7]
x2 = [2, 4, 6, 8, 10, 12]

plt.plot(y0, marker='o', label='0')
plt.plot(x1, y1, marker='H', linestyle=':', color='r', label='1')
plt.plot(x2, y2, marker='|', linestyle='-.', label='2')
plt.legend()
plt.show()
