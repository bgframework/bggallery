
Matplotlib gallery
==================

Using matplotlib v2.

Examples make use of the pyplot interface.
Check the Object Oriented Interface section for checking another approach.


Bars
    See :meth:`~matplotlib.axes.Axes.bar` and :meth:`~matplotlib.axes.Axes.barh`

Histograms
    See :meth:`~matplotlib.axes.Axes.hist`

Lines
    See :meth:`~matplotlib.axes.Axes.plot`

Scatter
    See :meth:`~matplotlib.axes.Axes.scatter`

