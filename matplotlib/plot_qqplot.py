"""

Q-Q plot
========

Q-Q plot against the null distribution

"""

import matplotlib.pyplot as plt
import numpy as np

pvalue_min = 0.001  # defined by your test
pvalues = np.concatenate([np.arange(1, 21) / 20, np.array([0.01, 0.015, 0, 0])])
pvalues[pvalues == 0] = pvalue_min

y = sorted(-np.log10(pvalues))
x = sorted(-np.log10(np.arange(1, len(pvalues) + 1) / float(len(pvalues))))


plt.scatter(x, y)

# Get the maximum pvalues (observed and expected)
max_x = np.max(x)
max_y = np.max(y)
# Give some extra space (5%)
max_x *= 1.1
max_y *= 1.1
line_values = np.linspace(0, np.floor(max(max_x, max_y)))
plt.plot(line_values, line_values, linestyle='--', color='m')
plt.show()
