
"""
Multiple bar plot
=================

Multiple bars in the same plot

"""

import matplotlib.pyplot as plt
import pandas as pd

df = pd.DataFrame({'G1': [20, 35, 30, 35, 27],
                   'G2': [25, 32, 34, 20, 25],
                   'G3': [3, 4, 7, 6, 2]})

index = df.index.values


bar_width = 0.15
group_start = (3 - 1) * bar_width / 2
plt.bar(index - group_start, df['G1'], width=bar_width, label='G1')
plt.bar(index - group_start + bar_width, df['G2'], width=bar_width, label='G2')
plt.bar(index - group_start + bar_width*2, df['G3'], width=bar_width, label='G3')
plt.legend()
plt.show()
