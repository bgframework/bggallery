
"""
Customizing your plots
======================

Adding customizations for your plots

.. contents::
   :local:

"""

# sphinx_gallery_thumbnail_number = 3

import matplotlib.pyplot as plt
import numpy as np


###############################################################################
# Adding title
# ------------

plt.plot([0, 5, 5])
plt.title('Fig title')
plt.show()


###############################################################################
# Adding labels to the axes
# -------------------------

plt.plot([1, 2, 2, 3, 3])
plt.xlabel('X-axis')
plt.ylabel('Y-axis')
plt.show()


###############################################################################
# Adding ticks to the axes
# ------------------------
#
# Set

plt.bar(np.arange(5), [3, 4, 7, 6, 2])
plt.xticks(np.arange(5), 'ABCDE')

###############################################################################
# Adding a grid
# -------------
#

plt.scatter(np.arange(5), [1, 5, 6, 2, 3])
plt.grid()
plt.show()


###############################################################################
# Adding a legend
# ---------------
#
# The legend is associated with the *labels*

plt.plot([1, 1, 1, 1, 1], label='ones')
plt.plot([0, 1, 2, 3, 4], label='line')
plt.legend()
plt.show()

###############################################################################
# Limiting the axis
# -----------------
#

plt.plot([0, 1, 2, 3, 4])
plt.xlim((1, 3))
plt.show()


###############################################################################
# Modifying the spines
# --------------------
#

_, ax = plt.subplots()
ax.plot([0, 1, 2, 3])
# Only draw spine between the y-ticks
ax.spines['left'].set_bounds(1, 2)
# Hide the right and top spines
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
plt.show()


###############################################################################
# Using rcParams
# --------------
#
# The matplolib parameters are set by style sheets.
# You can create you own style sheets or modify the parameters of one.
# To load a particular style sheet use :func:`~matplotlib.pyplot.style.use`.
# E.g.: :code:`plt.style.use('presentation')`
#
# To modify the style of a function, you can access :obj:`~matplotlib.rcParams`
# or use the :func:`~matplotlib.rc`
#
# .. tip:: use :func:`~matplotlib.rcdefaults` to restore the default values
#

import matplotlib

plt.rcParams['font.family'] = ['sans-serif']
plt.rcParams['font.sans-serif'] = ['arial']
plt.rcParams['font.size'] = 10
plt.rcParams['axes.labelsize'] = 10
plt.rcParams['xtick.labelsize'] = 10
plt.rcParams['ytick.labelsize'] = 10
plt.rcParams['axes.titlesize'] = 10
plt.rcParams['svg.fonttype'] = 'none'
plt.rcParams['mathtext.fontset'] = 'custom'
plt.rcParams['mathtext.cal'] = 'arial'
plt.rcParams['mathtext.rm'] = 'arial'
plt.rcParams['axes.edgecolor'] = 'black'
plt.rcParams['ytick.color'] = 'black'
plt.rcParams['axes.linewidth'] = 0.5
plt.rcParams['xtick.major.width'] = 0.5
plt.rcParams['ytick.major.width'] = 0.5
plt.rcParams['xtick.major.size'] = 3
plt.rcParams['ytick.major.size'] = 3

plt.plot([0, 1, 2, 3, 4])
plt.xlabel('X-axis')
plt.ylabel('Y-axis')
plt.show()

matplotlib.rcdefaults()