
"""
Some utilities
==============

.. contents::
   :local:

"""


###############################################################################
# Random color
# ------------

import numpy as np


def random_color():
    return np.random.rand(3)
