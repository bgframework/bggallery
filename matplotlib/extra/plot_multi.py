
"""

Multiple plots on the same figure
=================================

In matplotlib you can make use of the :func:`~matplotlib.pyplot.subplot` function.
Indicate number of rows, number of columns and the index of the subplot.

For more complicated arrangements see https://jakevdp.github.io/PythonDataScienceHandbook/04.08-multiple-subplots.html#plt.GridSpec:-More-Complicated-Arrangements

"""

import matplotlib.pyplot as plt


ax1 = plt.subplot(2, 2, 1)
ax1.plot([0, 1, 2])
ax2 = plt.subplot(2, 2, 2)
ax2.plot([2, 1, 0])
ax3 = plt.subplot(2, 2, 3)
ax3.plot([1, 2, 1])

plt.show()
