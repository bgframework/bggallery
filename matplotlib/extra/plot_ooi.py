"""

Object Oriented Interface
=========================

The use of the object oriented interface of matplotlib is quite easy.
You only need to replace most of the ``plt.`` calls you make for
similar calls on the axis you want to plot to.

There are few additional steps you need to do:

- create a figure
- add one or multiple subplots
- add a canvas to the figure to be able to plot it

"""


from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
from scrape import show

fig = Figure()
ax = fig.add_subplot(111)
ax.plot([1, 2, 3])
ax.set_title('Title')
ax.set_xlabel('X-axis')

FigureCanvas(fig)  # add a canvas. Now you can save the figure

show(fig)
