
"""
Stacked bar plot
================

.. hint:: use :func:`~numpy.cumsum` to compute the *bottom* or *left* arguments

"""


import matplotlib.pyplot as plt
import pandas as pd

df = pd.DataFrame({'G1': [20, 35, 30, 35, 27],
                   'G2': [25, 32, 34, 20, 25],
                   'G3': [3, 4, 7, 6, 2]})

index = df.index.values

plt.bar(index, df['G1'], label='G1')
plt.bar(index, df['G2'], bottom=df['G1'], label='G2')
plt.bar(index, df['G3'], bottom=df['G2'] + df['G1'], label='G3')
plt.legend()
plt.show()

