
"""
Venn digagrams
==============

In matplotlib venn diagrams can be done with the help of the
`matplotlib-venn <https://pypi.org/project/matplotlib-venn/>`_ library.


"""


from matplotlib import pyplot as plt
import numpy as np
from matplotlib_venn import venn2, venn2_circles


set1 = {'GATA3', 'TP53', 'BRAC1', 'PIK3CA'}
set2 = {'TP53', 'VHL', 'FLT3'}

only_1 = set1 - set2
only_2 = set2 - set1
common = set1 & set2

fig, ax = plt.subplots()
v = venn2([set1, set2], set_labels=('A', 'B'), ax=ax)
c = venn2_circles(subsets=(len(only_1), len(only_2), len(common)), linestyle='dashed', ax=ax)
c[0].set_lw(1.0)
c[0].set_ls('solid')

# Add annotations
annotations_kw = {'ha': 'center', 'textcoords': 'offset points',
                  'bbox': dict(boxstyle='round,pad=0.5', fc='gray', alpha=0.1),
                  'arrowprops': dict(arrowstyle='->', connectionstyle='arc3,rad=0.5', color='gray')}
if len(common) > 0:
    ax.annotate(', '.join(common), xy=v.get_label_by_id('11').get_position() + np.array([0, 0.05]),
                xytext=(0, -150), **annotations_kw)
if len(only_1) > 0:
    ax.annotate(', '.join(only_1), xy=v.get_label_by_id('10').get_position() - np.array([0, 0.05]),
                xytext=(-75, 75), **annotations_kw)
if len(only_2) > 0:
    ax.annotate(', '.join(only_2), xy=v.get_label_by_id('01').get_position() + np.array([0, 0.05]),
                xytext=(75, -75), **annotations_kw)

plt.show()
