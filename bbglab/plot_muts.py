"""

Plot mutations in gene
======================

Plot the mutations in a gene.

.. tip:: If you have many points in the same are you can add some diversion
   by adding a random value to the counts (a value between -0.4 and 0.4)

"""

import pandas as pd
from bokeh.plotting import figure
from bokeh.models import ColumnDataSource, HoverTool, BoxAnnotation, Range1d, NumeralTickFormatter
from scrape import show


chr_ = '1'
region_start = 90
region_end = 260
region_segments = [(100, 105), (200, 250)]

df = pd.DataFrame({'CHR': ['1'] * 7, 'POS': [101, 101, 135, 136, 220, 220, 220], 'REF': list('AACGTTT'), 'ALT': list('TTGCAAA')})

fig = figure()

# Count mutations
counts = df.groupby(['POS', 'REF', 'ALT'], as_index=False).size()
counts = pd.DataFrame(counts, columns=['COUNTS']).reset_index()


source = ColumnDataSource(counts)

f = fig.circle(source=source, x='POS', y='COUNTS', size=10)

# add tooltip
tip = [
    ("position", "@POS"),
    ("type", "@REF -> @ALT x @COUNTS")
]
hover = HoverTool(
    tooltips=tip,
    renderers=[f]
)
fig.add_tools(hover)

# Mark regions
for begin, end in region_segments:
    fig.add_layout(BoxAnnotation(left=begin, right=end, fill_alpha=0.1, line_color='olive', fill_color='olive'))

# Limit the x axis to the gene
fig.x_range = Range1d(region_start, region_end)

fig.xaxis[0].formatter = NumeralTickFormatter(format="0")
fig.ygrid.grid_line_alpha = 0.7
fig.ygrid.grid_line_dash = [6, 4]

fig.xaxis.axis_label = 'Position'
fig.yaxis.axis_label = 'Times observed'

show(fig)


###############################################################################
# If you are only interested in plotting the segments and forget about the rest


from intervaltree import IntervalTree
from bokeh.models import Span


class Distance:
    def __init__(self):
        self.value = 0


chr_ = '1'
is_strand_positive = False
region_segments = [(100, 105), (200, 250)]

df = pd.DataFrame({'CHR': ['1'] * 7, 'POS': [101, 101, 135, 136, 220, 220, 220], 'REF': list('AACGTTT'), 'ALT': list('TTGCAAA')})

# Build a tree with the segments
tree = IntervalTree()
for begin, end in region_segments:
    tree.addi(begin, end+1, Distance())
# Compute the relative distance between segments
stop = tree.begin()
relative_accumulated_distance = 0
for interval in sorted(tree):
    next_start = interval[0]
    next_stop = interval[1]
    relative_accumulated_distance += next_start - stop
    stop = next_stop
    interval.data.value = relative_accumulated_distance
max_distance = relative_accumulated_distance
# update negative strand interval distances
if not is_strand_positive:
    for interval in sorted(tree):
        interval.data.value = max_distance - interval.data.value


fig = figure()

# Remove positions not in the tree
df = df[df['POS'].apply(lambda x: bool(tree[x]))]

# Count mutations
counts = df.groupby(['POS', 'REF', 'ALT'], as_index=False).size()
counts = pd.DataFrame(counts, columns=['COUNTS']).reset_index()

# Remove positions not in the tree
counts = counts[counts['POS'].apply(lambda x: bool(tree[x]))]

# Compute the relative positions for the collapsed values
if is_strand_positive:
    counts['REL_POS'] = counts['POS'].map(lambda x: x - tree.begin() - next(iter(tree[x])).data.value)
else:
    counts['REL_POS'] = counts['POS'].map(lambda x: tree.end() - x - next(iter(tree[x])).data.value)
## Only one value is expected in the next(iter(tree[x])) operation

source = ColumnDataSource(counts)

f = fig.circle(source=source, x='REL_POS', y='COUNTS', size=10)

# add tooltip
tip = [
    ("position", "@POS"),
    ("type", "@REF -> @ALT x @COUNTS")
]
hover = HoverTool(
    tooltips=tip,
    renderers=[f]
)
fig.add_tools(hover)


# Mark regions
start = min([x for x, _ in region_segments])
end = max([x for _, x in region_segments])
for begin, end in region_segments:
    if begin == start:
        continue
    if is_strand_positive:
        pos = begin - tree.begin() - next(iter(tree[begin])).data.value
    else:
        pos = tree.end() - begin - next(iter(tree[begin])).data.value
    fig.add_layout(Span(location=pos, dimension='height', line_color='olive', line_dash='dashed', line_width=2))

# Limit the x axis to the gene
fig.x_range = Range1d(0, max_distance)

fig.xaxis[0].formatter = NumeralTickFormatter(format="0")
fig.ygrid.grid_line_alpha = 0.7
fig.ygrid.grid_line_dash = [6, 4]

fig.xaxis.axis_label = 'Position'
fig.yaxis.axis_label = 'Times observed'

show(fig)
