
"""
Enrichment plot
===============

An enrichment plot is a line plot over an enrichment table.
The only "tricky" part is to create such table.

"""

import matplotlib.pyplot as plt
import numpy as np


def compare(eval_list, compare_set, step=1, maximum=None):
    enrichment = []
    x = []
    maximum = maximum or len(eval_list)
    for rank in range(step, maximum, step):
        x.append(rank)
        if rank <= len(eval_list):
            enrichment.append(len(set(eval_list[:rank]) & compare_set) / rank)
        else:
            enrichment.append(np.nan)
    return x, enrichment


base = {'GATA3', 'TP53', 'PIK3CA', 'VHL', 'CDKN2A', 'KRAS', 'NCOR1', 'PTEN', 'BRCA2', 'FOXA1'}
res_1 = ['NRAS', 'ANK3', 'ASPM', 'ARID1A', 'GNAS'] + list(base)
res_2 = list(base) + ['NRAS', 'ANK3', 'ASPM', 'ARID1A', 'GNAS']

x1, y1 = compare(res_1, base, step=2)
plt.plot(x1, y1, label='1', marker='o')

x2, y2 = compare(res_2, base)
plt.plot(x2, y2, label='2', marker='|')
plt.legend()
plt.show()
