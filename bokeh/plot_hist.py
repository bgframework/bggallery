
"""
Histograms
==========

Bokeh does not have a function for histogram,
thus we will use :meth:`~bokeh.plotting.figure.Figure.quad`
and :func:`~numpy.histogram`

"""

import numpy as np
from bokeh.plotting import figure, show

data = [1, 2, 1, 3]
hist, edges = np.histogram(data, bins=7)

fig = figure()
fig.quad(top=hist, bottom=0, left=edges[:-1], right=edges[1:])
show(fig)
