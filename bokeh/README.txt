
Bokeh gallery
=============

Using bokeh 0.13


Bars
    See :meth:`~bokeh.plotting.figure.Figure.vbar` and :meth:`~bokeh.plotting.figure.Figure.hbar`
    For stacked bar plots see also :meth:`~bokeh.plotting.figure.Figure.vbar_stacked` and :meth:`~bokeh.plotting.figure.Figure.hbar_stacked`

Lines
    See :meth:`~bokeh.plotting.figure.Figure.line`

Scatter
    See :meth:`~bokeh.plotting.figure.Figure.circle`
