
"""
Multiple bar plot
=================

Multiple bars in the same plot

"""

import pandas as pd
from bokeh.plotting import figure, show

df = pd.DataFrame({'G1': [20, 35, 30, 35, 27],
                   'G2': [25, 32, 34, 20, 25],
                   'G3': [3, 4, 7, 6, 2]})

index = df.index.values

bar_width = 0.15
group_start = (3 - 1) * bar_width / 2
fig = figure()
fig.vbar(index - group_start, top=df['G1'], width=bar_width, legend='G1')
fig.vbar(index - group_start + bar_width, top=df['G2'], width=bar_width, legend='G2', color='red')
fig.vbar(index - group_start + bar_width*2, top=df['G3'], width=bar_width, legend='G3', color='green')
show(fig)
