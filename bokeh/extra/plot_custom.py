
"""
Customizing your plots
======================

Adding customizations for your plots

.. contents::
   :local:

"""

import numpy as np
from bokeh.plotting import figure, show

# sphinx_gallery_thumbnail_number = 3


###############################################################################
# Adding title
# ------------

fig = figure(title='Title')
fig.line(np.arange(3), [0, 5, 5])
show(fig)


###############################################################################
# Adding labels to the axes
# -------------------------

fig = figure()
fig.circle(np.arange(5), [1, 2, 2, 3, 3])
fig.xaxis.axis_label = "X-axis"
fig.xaxis.axis_label_text_color = "#aa6666"
fig.yaxis.axis_label = "Y-axis"
fig.yaxis.axis_label_text_font_style = "italic"
show(fig)


###############################################################################
# Adding ticks to the axes
# ------------------------


from bokeh.models import FuncTickFormatter
from bokeh.models.tickers import FixedTicker

x = np.arange(5)
y = [3, 4, 7, 6, 2]

fig = figure()
fig.circle(x, y)

ticker = FixedTicker(ticks=x)
fig.xaxis.ticker = ticker
formatter = FuncTickFormatter(code="""
        data = {}
        return data[tick]
    """.format({k: v for k, v in zip(x, 'ABCDE')}))
fig.xaxis.formatter = formatter

show(fig)



###############################################################################
# Mofifying the grid
# ------------------
#

fig = figure()
fig.circle_cross(np.arange(5), [1, 5, 6, 2, 3])
fig.grid.grid_line_alpha = 0.8
fig.grid.grid_line_dash = [6, 4]
show(fig)


###############################################################################
# Limiting the axis
# -----------------
#

from bokeh.models import Range1d

fig = figure()
fig.circle(np.arange(5), [0, 1, 2, 3, 4])
fig.x_range = Range1d(0.9, 3.1)
show(fig)
