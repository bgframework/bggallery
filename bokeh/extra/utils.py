
"""
Some utilities
==============

.. contents::
   :local:

"""


###############################################################################
# Random color
# ------------

import random


def random_color():
    """Generate a random color"""
    return '#%02X%02X%02X' % (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
