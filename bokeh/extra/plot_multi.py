
"""

Multiple plots on the same figure
=================================

In bokeh you can make use of :func:`~bokeh.layouts.column`, :func:`~bokeh.layouts.row` or
:func:`~bokeh.layouts.gridplot` to join multiple plos into one figure.

"""

from bokeh.layouts import gridplot
from bokeh.plotting import figure, show


x = list(range(11))
y0 = x
y1 = [10 - i for i in x]
y2 = [abs(i - 5) for i in x]

fig1 = figure()

fig1.circle(x, y0, size=7)
fig2 = figure()
fig2.triangle(x, y1, size=7)
fig3 = figure()
fig3.square(x, y2, size=7)

grid = gridplot([[fig1, fig2], [None, fig3]])
show(grid)
