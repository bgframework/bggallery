
"""
Stacked bar plot
================

Vertically stacked bars

"""

import pandas as pd
from bokeh.plotting import figure, show

df = pd.DataFrame({'G1': [20, 35, 30, 35, 27],
                   'G2': [25, 32, 34, 20, 25],
                   'G3': [3, 4, 7, 6, 2]})

index = df.index.values

fig = figure()
fig.vbar(x=index, top=df['G1'], width=0.25, legend='G1')
fig.vbar(x=index, top=df['G2'] + df['G1'], bottom=df['G1'], width=0.25, legend='G2', color='red')
fig.vbar(x=index, top=df['G3'] + df['G2'] + df['G1'], bottom=df['G2'] + df['G1'], width=0.25, legend='G3', color='green')
show(fig)
