bokeh           # 0.13.0
intervaltree    # 2.1.0
matplotlib      # 2.2.3
matplotlib_venn # 0.11.5
numpy           # 1.15.1
pandas          # 0.23.4
sphinx-gallery  # commit 6d0a0a69134f557b49023e734169ba6a47dbefda
#selenium=3.14.0
#phantomjs=2.1.1